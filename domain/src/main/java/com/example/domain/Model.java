package com.example.domain;

import com.google.gson.annotations.SerializedName;

public class Model {
    private final String pic;

    @SerializedName("name")
    private String name;

    @SerializedName("gmail")
    private String gmail;

    @SerializedName("username")
    private String username;

    @SerializedName("city")
    private String city;

    @SerializedName("gender")
    private  String gender;


    public Model(String name, String gmail, String username, String city, String gender,String pic) {
        this.name = name;
        this.gmail = gmail;
        this.username = username;
        this.city = city;
        this.gender = gender;
        this.pic=pic;
        
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGmail() {
        return gmail;
    }

    public void setGmail(String gmail) {
        this.gmail = gmail;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPic() {
        return pic;
    }
}
