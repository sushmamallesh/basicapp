package com.example.remote;


import com.example.data.JsonDataModel;
import com.example.domain.Model;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    public static List<Model> data = new ArrayList<>();


    public static void retrofitClient1() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://randomuser.me/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RetrofitInterface retrofitInterface = retrofit.create(RetrofitInterface.class);

        Call<JsonDataModel> call1 = retrofitInterface.getAllData();

        call1.enqueue(new Callback<JsonDataModel>() {
            @Override
            public void onResponse(Call<JsonDataModel> call, Response<JsonDataModel> response) {
                JsonDataModel cdata = response.body();

                for (int i = 0; i < cdata.getResults().size(); i++) {
                    data.add(new Model(cdata.getResults().get(i).getName().getTitle() + cdata.getResults().get(i).getName().getFirst() + cdata.getResults().get(i).getName().getLast(), cdata.getResults().get(i).getGender(), cdata.getResults().get(i).getEmail(), cdata.getResults().get(i).getLocation().getCity(), cdata.getResults().get(i).getLogin().getUsername(), cdata.getResults().get(i).getPicture().getLarge()));
                }
            }

            @Override
            public void onFailure(Call<JsonDataModel> call, Throwable t) {

            }
        });
    }
}
