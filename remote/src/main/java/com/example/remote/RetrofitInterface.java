package com.example.remote;

import com.example.data.JsonDataModel;

import retrofit2.Call;
import retrofit2.http.GET;

public interface RetrofitInterface {

    @GET("?results=10")
    Call<JsonDataModel> getAllData();
}
