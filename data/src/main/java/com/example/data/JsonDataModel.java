package com.example.data;

import com.example.basicapp.apidata.Coordinates;
import com.example.basicapp.apidata.Street;
import com.example.basicapp.apidata.Timezone;

import java.io.Serializable;
import java.util.List;

public class JsonDataModel implements Serializable {
    private List<? extends Results> results;

    public List<? extends Results> getResults() {
        return results;
    }

    public void setResults(List<? extends Results> results) {
        this.results = results;
    }

    public static class Results{

        private String nat;
        private String gender;
        private String phone;
        private Name name;
        private Location location;
        private Login login;
        private String cell;
        private String email;
        private Picture picture;

        public String getNat() {
            return nat;
        }

        public void setNat(String nat) {
            this.nat = nat;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public Name getName() {
            return name;
        }

        public void setName(Name name) {
            this.name = name;
        }

        public Location getLocation() {
            return location;
        }

        public void setLocation(Location location) {
            this.location = location;
        }

        public Login getLogin() {
            return login;
        }

        public void setLogin(Login login) {
            this.login = login;
        }

        public String getCell() {
            return cell;
        }

        public void setCell(String cell) {
            this.cell = cell;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public Picture getPicture() {
            return picture;
        }

        public void setPicture(Picture picture) {
            this.picture = picture;
        }

        public static class Name{
            private String first;
            private String last;
            private String title;

            public String getFirst() {
                return first;
            }

            public void setFirst(String first) {
                this.first = first;
            }

            public String getLast() {
                return last;
            }

            public void setLast(String last) {
                this.last = last;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }
        }

        public static class Location {
            private String country;
            private String city;
            private Street street;
            private Timezone timezone;
            private int postcode;
            private Coordinates coordinates;
            private String state;

            public String getCountry() {
                return country;
            }

            public void setCountry(String country) {
                this.country = country;
            }

            public String getCity() {
                return city;
            }

            public void setCity(String city) {
                this.city = city;
            }

            public Street getStreet() {
                return street;
            }

            public void setStreet(Street street) {
                this.street = street;
            }

            public Timezone getTimezone() {
                return timezone;
            }

            public void setTimezone(Timezone timezone) {
                this.timezone = timezone;
            }

            public int getPostcode() {
                return postcode;
            }

            public void setPostcode(int postcode) {
                this.postcode = postcode;
            }

            public Coordinates getCoordinates() {
                return coordinates;
            }

            public void setCoordinates(Coordinates coordinates) {
                this.coordinates = coordinates;
            }

            public String getState() {
                return state;
            }

            public void setState(String state) {
                this.state = state;
            }
        }

        public static class Login  {

            private String sha1;
            private String password;
            private String salt;
            private String sha256;
            private String uuid;
            private String username;
            private String md5;

            public String getSha1() {
                return sha1;
            }

            public void setSha1(String sha1) {
                this.sha1 = sha1;
            }

            public String getPassword() {
                return password;
            }

            public void setPassword(String password) {
                this.password = password;
            }

            public String getSalt() {
                return salt;
            }

            public void setSalt(String salt) {
                this.salt = salt;
            }

            public String getSha256() {
                return sha256;
            }

            public void setSha256(String sha256) {
                this.sha256 = sha256;
            }

            public String getUuid() {
                return uuid;
            }

            public void setUuid(String uuid) {
                this.uuid = uuid;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            public String getMd5() {
                return md5;
            }

            public void setMd5(String md5) {
                this.md5 = md5;
            }
        }

        public static class Picture {
            private String thumbnail;
            private String large;
            private String medium;


            public String getThumbnail() {
                return thumbnail;
            }

            public void setThumbnail(String thumbnail) {
                this.thumbnail = thumbnail;
            }

            public String getLarge() {
                return large;
            }

            public void setLarge(String large) {
                this.large = large;
            }

            public String getMedium() {
                return medium;
            }

            public void setMedium(String medium) {
                this.medium = medium;
            }
        }
    }
}


