package com.example.basicapp.presentation.view;

import static com.example.basicapp.presentation.view.MainActivity.data;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.basicapp.databinding.FragmentHomeBinding;
import com.example.basicapp.Adapter;

public class HomeFragment extends Fragment {


    public HomeFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

//        return inflater.inflate(R.layout.fragment_home, container, false);
        FragmentHomeBinding fragmentHomeBinding=FragmentHomeBinding.inflate(inflater,container,false);
        View view = fragmentHomeBinding.getRoot();
        Adapter adapter = new Adapter(getContext(),data);
        fragmentHomeBinding.rvRecyclerView.setAdapter(adapter);
        return view;

    }
}