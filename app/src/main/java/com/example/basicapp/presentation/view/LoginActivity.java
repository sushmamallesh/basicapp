package com.example.basicapp.presentation.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.example.basicapp.R;
import com.example.basicapp.databinding.ActivityLoginBinding;

import java.util.regex.Pattern;


public class LoginActivity extends AppCompatActivity {

    ActivityLoginBinding activityLoginBinding;
    public static String email;
    public static String password;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_login);

        activityLoginBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        activityLoginBinding.btlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                email = activityLoginBinding.etemail.getEditText().getText().toString();
                password = activityLoginBinding.etpassword.getEditText().getText().toString();
                if (!Pattern.matches("[a-zA-Z0-9]{1,}[@]{1}[a-zA-Z]{1,}.{1}[a-z]{2,}", email)) {
                    Toast.makeText(LoginActivity.this, "enter email", Toast.LENGTH_SHORT).show();
                } else if (!Pattern.matches("[a-zA-Z0-9@#$]{6,}", password)) {
                    Toast.makeText(LoginActivity.this, "enter password", Toast.LENGTH_SHORT).show();
                } else {
                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                }
            }
        });

    }
    public void openSignUp(View view) {
        startActivity(new Intent(LoginActivity.this, SignUpActivity.class));
    }

}
