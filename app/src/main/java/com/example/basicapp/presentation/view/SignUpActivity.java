package com.example.basicapp.presentation.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.example.basicapp.R;
import com.example.basicapp.databinding.ActivitySignUpBinding;

import java.util.regex.Pattern;

public class SignUpActivity extends AppCompatActivity {
    ActivitySignUpBinding activitySignUpBinding;

    public static  String firstName;
    public static String lastName;
    public static String email;
    public static String password;
    public static String phoneNumber;
    public static String confirmPassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_sign_in);
        activitySignUpBinding = DataBindingUtil.setContentView(this, R.layout.activity_sign_up);

        activitySignUpBinding.btcontinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                email = activitySignUpBinding.etemail.getEditText().getText().toString();
                password = activitySignUpBinding.etsignpassword.getEditText().getText().toString();
                firstName = activitySignUpBinding.etfirstname.getEditText().getText().toString();
                lastName = activitySignUpBinding.etlastname.getEditText().getText().toString();
                phoneNumber = activitySignUpBinding.etphone.getEditText().getText().toString();
                confirmPassword = activitySignUpBinding.etsignconfrmpassword.getEditText().getText().toString();
                boolean accept = activitySignUpBinding.cbaccept.isChecked();

                if (TextUtils.isEmpty(firstName) || TextUtils.isEmpty(lastName) || TextUtils.isEmpty(phoneNumber) || TextUtils.isEmpty(password) || TextUtils.isEmpty(confirmPassword)) {
                    Toast.makeText(SignUpActivity.this, "fields are missing", Toast.LENGTH_SHORT).show();
                } else if (!TextUtils.equals(password, confirmPassword)) {
                    Toast.makeText(SignUpActivity.this, "enter password correctly", Toast.LENGTH_SHORT).show();
                } else if (!accept) {
                    Toast.makeText(SignUpActivity.this, "accept terms and conditions", Toast.LENGTH_SHORT).show();
                }else if(!Pattern.matches("[a-zA-Z]{2,}",firstName)){
                    Toast.makeText(SignUpActivity.this,"enter firstname", Toast.LENGTH_SHORT).show();
                }else if(!Pattern.matches("[a-zA-Z0-9@#$]{6,}",password)){
                    Toast.makeText(SignUpActivity.this, "enter password", Toast.LENGTH_SHORT).show();
                }else if(!Pattern.matches("[0-9]{10}",phoneNumber)){
                    Toast.makeText(SignUpActivity.this, "enter phone", Toast.LENGTH_SHORT).show();
                }
                else if(!Pattern.matches("[a-zA-Z0-9]{1,}[@]{1}[a-zA-Z]{1,}.{1}[a-z]{2,}",email)){
                    Toast.makeText(SignUpActivity.this,"enter email", Toast.LENGTH_SHORT).show();
                }
                else {
                    startActivity(new Intent(SignUpActivity.this, MainActivity.class));
                }
            }
        });

    }


}

