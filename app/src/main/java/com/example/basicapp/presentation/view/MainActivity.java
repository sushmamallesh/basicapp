package com.example.basicapp.presentation.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.view.MenuItem;

import com.example.basicapp.R;
//import com.example.basicapp.domain.Model;
import com.example.domain.Model;

import com.example.remote.RetrofitInterface;
import com.example.data.JsonDataModel;
import com.google.android.material.bottomnavigation.BottomNavigationView;

//import com.example.basicapp.data.RetrofitInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    public static List<Model> data = new ArrayList<>();
    BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


//        RetrofitClient.retrofitClient1();
        retrofitClient();
        getSupportFragmentManager().beginTransaction().add(R.id.fragContainer, new HomeFragment()).commit();
//        getSupportFragmentManager().beginTransaction().add(R.id.fragContainer,new ProfileFragment()).commit();
//        getSupportFragmentManager().beginTransaction().add(R.id.fragContainer,new SearchFragment()).commit();

        bottomNavigationView = findViewById(R.id.btnavigation);
        bottomNavigationView.setSelectedItemId(R.id.home);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.home:
                        getSupportFragmentManager().beginTransaction().replace(R.id.fragContainer, new HomeFragment()).commit();
                        return true;
                    case R.id.profile:
                        getSupportFragmentManager().beginTransaction().replace(R.id.fragContainer, new ProfileFragment()).commit();
                         return true;
                    case R.id.search:
                        getSupportFragmentManager().beginTransaction().replace(R.id.fragContainer, new SearchFragment()).commit();
                        return true;
                }
                return false;
            }
        });


    }

    public void retrofitClient(){
        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://randomuser.me/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RetrofitInterface retrofitInterface = retrofit.create(RetrofitInterface.class);


        Call<JsonDataModel> call=retrofitInterface.getAllData();
        call.enqueue(new Callback<JsonDataModel>() {
            @Override
            public void onResponse(Call<JsonDataModel> call, Response<JsonDataModel> response) {
                JsonDataModel cdata = response.body();

                for(int i=0; i<cdata.getResults().size(); i++) {
                    data.add(new Model(cdata.getResults().get(i).getName().getTitle()+cdata.getResults().get(i).getName().getFirst()+cdata.getResults().get(i).getName().getLast(), cdata.getResults().get(i).getGender(), cdata.getResults().get(i).getEmail(), cdata.getResults().get(i).getLocation().getCity(), cdata.getResults().get(i).getLogin().getUsername(), cdata.getResults().get(i).getPicture().getLarge()));
                }
                getSupportFragmentManager().beginTransaction().replace(R.id.fragContainer, new HomeFragment()).commit();
            }

            @Override
            public void onFailure(Call<JsonDataModel> call, Throwable t) {

            }
        });
    }


}

