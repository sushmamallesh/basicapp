package com.example.basicapp.apidata;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface JsonPlaceHolderApi {

    @GET("resultsItem")
    Call<List<ResultsItem>> getResulsItem();
}
