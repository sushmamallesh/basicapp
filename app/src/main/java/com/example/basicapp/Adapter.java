package com.example.basicapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.basicapp.databinding.ActivityRecyclerViewBinding;
import com.example.domain.Model;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.util.List;
public class Adapter extends androidx.recyclerview.widget.RecyclerView.Adapter<Adapter.MyViewHolder> {

    List<Model> listModel;
    Context context;

    public Adapter(Context context,List<Model> data) {
        this.listModel = data;
        this.context = context;
    }

    @NonNull
    @Override
    public Adapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        ActivityRecyclerViewBinding activityRecyclerViewBinding =ActivityRecyclerViewBinding.inflate(layoutInflater,parent,false);
        return new MyViewHolder(activityRecyclerViewBinding);

    }

    @Override
    public void onBindViewHolder(@NonNull Adapter.MyViewHolder holder, int position) {
        holder.activityRecyclerViewBinding.setModel(listModel.get(position));
        holder.activityRecyclerViewBinding.executePendingBindings();

        Picasso.Builder builder = new Picasso.Builder(context);
        builder.downloader(new OkHttp3Downloader(context));
        builder.build().load(listModel.get(position).getPic())
                .into(holder.activityRecyclerViewBinding.ivprofilephoto);
    }

    @Override
    public int getItemCount() {
        return listModel.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ActivityRecyclerViewBinding activityRecyclerViewBinding;
        public MyViewHolder(ActivityRecyclerViewBinding activityRecyclerViewBinding
        ) {
            super(activityRecyclerViewBinding.getRoot());
            this.activityRecyclerViewBinding = activityRecyclerViewBinding;
            
        }
    }
}
